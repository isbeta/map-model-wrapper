#from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_decomposition import PLSRegression
from sklearn import svm
import pandas as pd
import numpy as np


DEBUG = 0


class ConformalPredictor:
    '''Does the work yada yada....
       Describe format model expected in...
    '''

    def __init__(self, model_dict):
        self.model = model_dict['model']        # scikit-learn model object 
        self.train = model_dict['trainset']     # pandas data frame expected
        self.descriptors = model_dict['descriptors']
   
        # calculate probability distribution of nonconformity scores from training set 
        self.DModX = self.calc_DModX(self.train, self.model, self.descriptors)
        self.error_model = self.create_error_model(self.train, self.model)
        self.P = self.calc_P_dist()



    def calc_DModX(self, df, m, descriptors):
        '''Expects pandas dataframe and scikit-learn PLSRegression model.
           It's REALLY important that the 'descriptors' attribute lists the descriptors in the 
           same order they appear in the dataframe.'''

        X = df[descriptors]
        T = m.x_scores_
        P = m.x_loadings_
        E = T.dot(P.T) - X
        DModX = E.std(axis=1)

        return DModX



    def create_error_model(self, df, m):
        '''Expects pandas dataframe and scikit-learn PLSRegression model.'''

        vars = pd.DataFrame(m.x_scores_)
        vars['DModX'] = pd.Series(self.DModX, index=vars.index)
        pred = self.model.predict(self.train[self.descriptors])[0]
        resid = abs(pred - df['YVar'])

        model = svm.SVR()
        model.fit(vars, resid)

        return model



    def calc_P_dist(self):
        '''Calculates (sorted) probability distribution of nonconformities for training set.'''

        features = self.train[self.descriptors]
        pred = self.model.predict(features)[0]
        vars = pd.DataFrame(self.model.x_scores_)
        vars['DModX'] = pd.Series(self.DModX, index=vars.index)

        E = self.error_model.predict(vars)
        P = abs(self.train['YVar'] - pred) / E
        P.sort_values(inplace=True)

        return P.tolist()
      


    def Predict(self, features, confidence=0.95):
        '''Actual call for making predictions using the model.  "features" is a simple list of the 
           descriptor values (not names, not a named list).
        '''

        X = np.reshape(features, (1, -1))
        [pred] = self.model.predict(X)[0]

        # get predicted DModX
        T = self.model.transform(X)
        P = self.model.x_loadings_
        E = T.dot(P.T) - X
        DModX = E.std(axis=1)
        vars = pd.DataFrame(T)
        vars['DModX'] = pd.Series(self.DModX, index=vars.index)

        # get predicted residual
        error = self.error_model.predict(np.reshape(vars, (1, -1)))[0]

        # determine normalized error
        index = int( confidence * len(self.P) ) + 1
        if index > len(self.P)-1: index = len(self.P)-1
        error_bar = error * self.P[index]

        # return prediction, error estimate (alpha in this implementation), prediction confidence
        return pred, error_bar




if __name__ == "__main__":
  '''Need to create CP object for each compound in train set, excluding that compound in turn'''

  import cPickle as pickle

  delta = 0.95

  # input_file = "../DUMMY/ATI9G91_PLS.csv"
  model_fname = "/apps/www/models/ATI9G91_PAMPA.pkl"

  mfile = open(model_fname)
  m = pickle.load(mfile)
  mfile.close()

 
  model = m['model']
  df = m['trainset']
  

  cp = ConformalPredictor(m)  # should make a new one for each prediction...
  for idx in df.index:
      # print df.ix[idx, 'Observation']
      desc = []
      for dude in m['descriptors']:
          desc.append(df.ix[idx, dude])
      df.ix[idx, 'Prediction'],df.ix[idx, '95_conf_interval'] = cp.Predict(desc, delta)


  df['correct'] = abs(df['YVar'] - df['Prediction']) <= df['95_conf_interval']
  df.to_csv('./cp_dummy_test.csv')
  print df
  print df.groupby('correct').count()

    
