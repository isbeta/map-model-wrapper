'''
Created on August 27th, 2016

@author: manchjo2

Based on Niko Fechner's generic_model.py
'''
'''
General information

This codes provides a generic CIx webservice endpoint for MAP local QSAR models.  

MODEL is expected to be a pickled or joblib-dumped file which results into a dict-like 
object during loading (as in Niko's original implementation, but with additional elements).

    MODEL {
      'model' : serialized scikit-learn model providing predict_proba method,,
      'descriptors' : list of RDKit DescriptorEngine descriptor names required by model 
                      in exactly the same order as in training,
      'trainset' : pandas dataframe containing ID, SMILES, Yvar, Ypred, and descriptors 
    }

'''


import os, json, collections
import cPickle as pickle
import numpy as np
from flask import Flask
from flask import request

from xml.etree import cElementTree as ElementTree
from CIxFramework.Helpers import xmlHandlingExtra

from CIxFramework.Helpers import molHandling, djangoHandling, xmlHandling
from sklearn.externals import joblib
import urllib
import conformal

import sys, traceback

app = Flask(__name__)
app.debug = True    # auto-reload on code change

_version = 0.1

# default_model_folder = '/da/CIX/QSAR/example/'
default_model_folder = '/apps/www/models/'


@app.route("/")
def hello():
    return "You are at mapcix root: %s" % (__file__)

def _getModelFolder():
  return os.environ.get('QSARModelFolder', default_model_folder)

@app.route('/local/getVersion/', methods=['POST', 'GET'])
def getVersion():
  """ Version information of QSAR service """ 
  # return HttpResponse(json.dumps(_version), content_type='application/json')
  return json.dumps(_version)

@app.route('/local/help/', methods=['POST', 'GET'])
def getHelp():
  """CIX INTROSPECTION - exposed by ./help WS endpoint
  Display help information about QSAR service
  """
  s = {'name': 'MAP local model service',
       'description': 
            'Service to apply a local QSAR model specified by parameter. It is not supposed to be consumed by clients', }
  # return HttpResponse(json.dumps(s), content_type='application/json')
  return json.dumps(s)

@app.route('/local/getParameters/', methods=['POST', 'GET'])
# def getParameters(request):
def getParameters():
  """CIX INTROSPECTION - exposed by ./getParameters WS endpoint
  The QSAR service requires parameters """

  available_models = []
  for file in os.listdir(default_model_folder):
    available_models.append(file.split('.')[0])
  
  response = json.dumps(
                                  dict(
#                                       type=dict(type='choice', default='default', optional=1,
#                                                 description='prediction type',
#                                                 choices=['default', 'conformal']),
                                        model=dict(type='choice', default='', optional=0,
                                                  description='model name',
                                                  choices=available_models),
                                        confidence_level=dict(type='string', default='0.95', optional=1,
                                                  description='confidence level'),
                                       )
                   )
  return response


@app.route('/local/getReturnValues/', methods=['POST', 'GET'])
def getReturnValues():
  """CIX INTROSPECTION - exposed by ./getReturnValues WS endpoint
  The QSAR service returns the predicted property of interest along with error assessment """
  return json.dumps('')


def predict(etree, cp, descDict, descriptors, params=None):
  res = {}
  parameters = {}
  if params is not None:
    parameters = params

  # determine confidence level
  try:
    confidence = float(parameters['confidence_level'])
  except:
    confidence = 0.95

  # get result label
  if 'result_label' in parameters:
    res_label = 'map:' + parameters['result_label']
    res_error_label = 'map:' + parameters['result_label'] + '_%s_conf_interval' % confidence
  else:
    res_label = 'Prediction'
    res_error_label = 'Confidence_interval'
  
  #get descriptorEngine status reports for each Id
  status = {}
  cix_comment = {}
  for item in etree.find('content'):
      tid = item.get('id', None)
      if tid is not None:
        status[tid] = item.findtext('status')
        cix_comment[tid] = item.findtext('comment')
        
  for ID in descDict.keys():
    data = descDict[ID]
    res[ID] = {}
    if status[ID] == 'ERROR':
      comment = cix_comment[ID]
      if 'descError' in descDict[ID]:
        comment = comment + ';' + descDict[ID]['descError']
      #item-level error reporting  
      res[ID] = {'cix_status': 'ERROR', 'cix_comment':comment}
      continue
    
    try:
      features = [float(data[d]) for d in descriptors]
      p,e = cp.Predict(features, confidence)
      res[ID][res_label] = str(p)
      res[ID][res_error_label] = str(e)
    except :  #pylint: disable=W0702
      #this happens only if the descriptorEngine did not return an error state, but the prediction step failed
      exc_type, exc_value, exc_traceback = sys.exc_info()
      traceback.print_exception(exc_type, exc_value, exc_traceback)
      #item-level error reporting
      res[ID] = {'cix_status': 'ERROR', 'cix_comment':'Error in prediction'}
      xmlHandling.setOverallStatusAndCommentIn(etree, 'WARNING', 'Some predictions caused errors', False)

  return res



     
def _processRequestedDescriptors(descriptors):
  """
  Extends the descriptors to generally be able to handle fingerprints of various bit-lengths.
  This is not covered by the DescriptorEngine introspection, thus it has to be provided during model application
  """
  fingerprintDescriptors = []
  standardDescriptors = []
  for d in descriptors:
    if ':FP' in d:
      parts = d.split('_')
      if len(parts) != 3:
        print >>sys.stderr, 'Unusual descriptor name ',d
        if d not in standardDescriptors:
          standardDescriptors.append(d)
      else:
        title = parts[0]
        bitsize = int(parts[1])
        bit = int(parts[2])
        if title not in fingerprintDescriptors:
          fingerprintDescriptors.append(title)
    else:
      if d not in standardDescriptors:
          standardDescriptors.append(d)
  standardDescriptors.extend(fingerprintDescriptors)
  return standardDescriptors
                      

def getElementTreeFromPost(req):
    """ Extracts the xml information from request and converts to ElementTree """
    post = req.form['xml']
#   iamToken = req.cookies.get('ObSSOCookie')
    etree = ElementTree.fromstring(post)
#   if iamToken is not None:
#       xmlHandlingExtra.addToNode(etree, 'request', 'iamToken', iamToken, attribute='ObSSOCookie')
    return etree



@app.route('/local/', methods=['POST','GET'])
# def calcQSARModel(etree, molDict, paramDict):
def calcQSARModel():
  """ Entrypoint for webservice - exposed by WS endpoint 
  etree is an xml tree object that is used to store and transfer data within the cix framework:
  - for convenience the compound data is additionally provided as a python dictionary - molDict
  - the computed results are added to the etree object and return to the caller
  - the etree also contains specific parts for message passing, both per request and per item, that allow passing error information back to the client 
  """

  etree = getElementTreeFromPost(request)
  paramDict = xmlHandling.getModelParamsFromElementTree(etree)
  compoundType = xmlHandling.getModelMetaFromElementTree(etree)['modelInputMolFormat']
  molDict = xmlHandling.getCompoundsFromElementTree(etree, compoundType, 'modelInput')

  try:
    from rdkit.Novartis import DescriptorEngine
    from rdkit.Novartis.DescriptorEngine.dispatcher import fingerprintDispatcher
  except:
    xmlHandling.setOverallStatusAndCommentIn(etree, 'ERROR', 'Could not import rdkit', False)
    return etree
  
  #load a model
  try:
    if os.path.exists(paramDict['model']):
      model = joblib.load(paramDict['model'])
    elif os.path.exists(paramDict['model'] + '.pkl'):
      mfile = open(paramDict['model'] + '.pkl')
      model = pickle.load(mfile)
      mfile.close()
    elif os.path.exists(_getModelFolder() + paramDict['model']):
      model = joblib.load(_getModelFolder() + paramDict['model'])
    else:
      mfile = open(_getModelFolder() + paramDict['model'] + '.pkl')
      model = pickle.load(mfile)
      mfile.close()
  except:
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback)
    #add request-level error message
    xmlHandling.setOverallStatusAndCommentIn(etree, 'ERROR', 'Model not available', False)
    return etree

  # create a new ConformalPredictor engine/wrapper for model
  cp = conformal.ConformalPredictor(model)

  result_label = os.path.basename(paramDict['model'])
  paramDict['result_label'] = result_label

  #get required descriptors 
  try:
    if 'descriptors' in  model:
      descriptors = model['descriptors']
    else:
      descriptors = model.descriptors
    #sometimes '' is stored as descriptor, have to track that down, but until that, just catch this case here
    if '' in descriptors:
      descriptors.remove('')
    descs = descriptors
    
  except:
    if 'descriptors' in paramDict:
      descriptors = paramDict['descriptors'] 
      descs = descriptors.split(',')
    else:
      xmlHandling.setOverallStatusAndCommentIn(etree, 'ERROR', 'Descriptors neither specified in model nor provided as parameter', False)
    return etree
  
  #parse input smiles
  results = dict((k, dict(smiles=v['smiles'])) for k, v in molDict.items())
  if not molHandling.has_rdkitMol(results): molHandling.add_rdkitMol(results)

  #compute descriptors
  descriptors = _processRequestedDescriptors(descs)
  dispatcherFactory = DescriptorEngine.defaultDispatcherFactory()
  
  try:
    nBits = int(paramDict.get('nBits',1024))
    dispatcherFactory.registerDispatcher(fingerprintDispatcher.FingerprintDispatcher(fpNbits=nBits))
  except:
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback)
    xmlHandling.setOverallStatusAndCommentIn(etree, 'ERROR', 'Error during fingerprinter initialization', False)
    return etree
    
  descriptorEngine = dispatcherFactory.getDescriptorCalculator(','.join(descriptors))
  if descriptorEngine: descriptorEngine.calculate(results)
  for ID, value in results.items():
    del value['rdkitMol']
    for d in value:
      value[d] = str(value[d])
  
  xmlHandling.addDescCalcResultsTo(etree, results) 
  descDict = xmlHandling.getResultsFromElementTree(etree)
  xmlHandling.clearResultsIn(etree)
  
  #apply model using the computed descriptors
  res = predict(etree, cp, descDict, descs, paramDict)    
  
  xmlHandling.addDescCalcResultsTo(etree, res)
  return ElementTree.tostring(etree)



if __name__ == '__main__':
  app.run( 
        host="0.0.0.0",
        port=int("8000")
  )
